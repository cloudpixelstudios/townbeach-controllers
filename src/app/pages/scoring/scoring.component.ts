import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { take } from 'rxjs/operators';
import { Socket } from 'ngx-socket-io';

@Component({
  selector: 'app-scoring',
  templateUrl: './scoring.component.html',
  styleUrls: ['./scoring.component.scss']
})
export class ScoringComponent implements OnInit {

  public selectedField;
  public day;
  public week;
  public kokomoGames = [];
  public bondiGames = [];
  public selectedGame;
  public loadingTeams = true;
  public gameStarted = false;
  public screens = this.socket.fromEvent<string[]>('scoreboards');
  public online = this.socket.fromEvent<boolean>('online');
  public remoteScreens;
  public offlineMode = false;
  public backButtonAvaliable = false;
  public dummyGame = [{team_name: "Team 1", uid: "1"}, {team_name: "Team 2", uid: "2"}, "custom"];
  public secretClicks = 0;
  public secretTimerRunning = false;
  public switched = false;

  constructor(public _data: DataService, private socket: Socket) {
    let that = this;
    that.setTodaysGames();
    this.screens.subscribe(data => {
      this.remoteScreens = data;
    });
    this.online.subscribe(isOnline => {
      if(!isOnline) {
        this.offlineMode = true;
      }
    });
  }

  navigateBack() {
    if(this.selectedField != undefined && this.selectedGame != undefined) {
      let screen = this.remoteScreens[this.selectedField]
      this.selectedGame = undefined;
      this.socket.emit('preGame', {team_one_name: screen.team_one.team_name, team_two_name: screen.team_two.team_name, team_one_uid: screen.team_one.uid, team_two_uid: screen.team_one.uid, screen_id: this.selectedField, game_uid: screen.game_uid});
    } else if (this.selectedField != undefined && this.selectedGame == undefined){
      this.socket.emit('returnHome', {screen_id: this.selectedField});
      this.selectedField = undefined;
      this.backButtonAvaliable = false;
    }
  }

  setTodaysGames() {
    let that = this;
    let today = new Date();
    that.day = today.toLocaleString('en-us', {  weekday: 'long' });
    //that.day = "Wednesday";
    that._data.seasons.pipe(take(2)).subscribe(season => {
      if(season) {
        for(let i = 0; i < season.length; i++) {
          let dateString = that._data.convertDate(season[i].startDate);
          let weekStartDate = new Date(dateString);
          let weekEndDate = new Date(that._data.convertDate(that.addDay(weekStartDate, 7)));
          if(today > weekStartDate && today < weekEndDate) {
            that.week = season[i];
            switch(that.day) {
              case "Monday":
                that.kokomoGames = that.week.mondayKokomo;
                that.bondiGames = that.week.mondayBondi;
                break;
              case "Tuesday":
                that.kokomoGames = that.week.tuesdayKokomo;
                that.bondiGames = that.week.tuesdayBondi;
                break;
              case "Wednesday":
                that.kokomoGames = that.week.wednesdayKokomo;
                that.bondiGames = that.week.wednesdayBondi;
                break;
            }
            that.kokomoGames.push([{team_name: "Team 1", uid: "1"}, {team_name: "Team 2", uid: "2"}, "custom"])
            that.bondiGames.push([{team_name: "Team 1", uid: "1"}, {team_name: "Team 2", uid: "2"}, "custom"])
            that.loadingTeams = false;
          }
        }
      }
    })
  }

  switchSides() {
    this.switched = !(this.switched);
  }

  preGame(team_one_name, team_two_name, team_one_uid, team_two_uid, screen_id, game_uid) {
    this.backButtonAvaliable = true;
    this.socket.emit('preGame', {team_one_name: team_one_name, team_two_name: team_two_name, team_one_uid: team_one_uid, team_two_uid: team_two_uid, screen_id: screen_id, game_uid: game_uid});
  }

  startGame(screen_id) {
    this.socket.emit('startGame', {screen_id: screen_id});
    this.gameStarted = true;
  }

  addPoint(uid) {
    if(uid == this.selectedGame[0].uid) {
      this.socket.emit('updatePoints', {screen_id: this.selectedField, team_one_points: this.remoteScreens[this.selectedField].team_one.score + 1, team_two_points: this.remoteScreens[this.selectedField].team_two.score});
    } else {
      this.socket.emit('updatePoints', {screen_id: this.selectedField, team_one_points: this.remoteScreens[this.selectedField].team_one.score, team_two_points:  this.remoteScreens[this.selectedField].team_two.score + 1});
    }
  }

  takePoint(uid) {
    if(uid == this.selectedGame[0].uid) {
      if (!(this.remoteScreens[this.selectedField].team_one.score == 0)) {
        this.socket.emit('updatePoints', {screen_id: this.selectedField, team_one_points: this.remoteScreens[this.selectedField].team_one.score - 1, team_two_points: this.remoteScreens[this.selectedField].team_two.score});
      }
    } else {
      if (!(this.remoteScreens[this.selectedField].team_two.score == 0)) {
        this.socket.emit('updatePoints', {screen_id: this.selectedField, team_one_points: this.remoteScreens[this.selectedField].team_one.score, team_two_points: this.remoteScreens[this.selectedField].team_two.score - 1});
      }
    }
  }

  endGame(screen_id) {
    let that = this;
    if (window.confirm("Are you sure? This can not be undone.")) {
      this.backButtonAvaliable = false;
      let screen = this.remoteScreens[this.selectedField];
      let team_one_points = 0;
      let team_two_points = 0;
      if(screen.game_uid != "custom") {
        if(screen.team_one.team_uid == "") {
          team_two_points += 10;
        } else if (screen.team_two.team_uid == "") {
          team_one_points += 10;
        } else {
          if(screen.team_one.score > screen.team_two.score) {
            team_one_points += 10;
            team_two_points += 5;
          } else if (screen.team_one.score < screen.team_two.score) {
            team_two_points += 10;
            team_one_points += 5;
          } else {
            team_two_points += 7;
            team_one_points += 7;
          }
        }
        team_one_points = team_one_points + this.getExtraPoints(screen.team_one.score);
        team_two_points = team_two_points + this.getExtraPoints(screen.team_two.score);
      }
      if(screen.game_uid != "custom") {
        that._data.uploadResults(screen.game_uid, team_one_points, team_two_points, screen.team_one.score, screen.team_two.score, screen.team_one.team_uid, screen.team_two.team_uid).then(() => {
          that.socket.emit('endGame', {screen_id: screen_id, team_one: {score: screen.team_one.score, points: team_one_points}, team_two: {score: screen.team_two.score, points: team_two_points}, finish_time: new Date(), game_uid: screen.game_uid});
        })
      }
      this.selectedField = undefined;
      this.selectedGame = undefined;
      this.gameStarted = false;
      if(screen.game_uid != "custom") {
        that._data.uploadResults(screen.game_uid, team_one_points, team_two_points, screen.team_one.score, screen.team_two.score, screen.team_one.team_uid, screen.team_two.team_uid).then(() => {
          that.socket.emit('endGame', {screen_id: screen_id, team_one: {score: screen.team_one.score, points: team_one_points}, team_two: {score: screen.team_two.score, points: team_two_points}, finish_time: new Date(), game_uid: screen.game_uid});
        })
      } else {
        that.socket.emit('endGame', {screen_id: screen_id, team_one: {score: screen.team_one.score, points: team_one_points}, team_two: {score: screen.team_two.score, points: team_two_points}, finish_time: new Date(), game_uid: screen.game_uid});
      }
    }
  }

  resetScoreboard() {
    let that = this;
    this.secretClicks += 1;
    if(!this.secretTimerRunning) {
      this.secretTimerRunning = true;
      setTimeout(function() {
        that.secretClicks = 0;
        that.secretTimerRunning = false;
      }, 3000)
    }
    if(this.secretClicks >= 3) {
      let screen = this.remoteScreens[this.selectedField];
      if (confirm("OK for Kokomo, CANCEL for Bondi") ) {
        if (confirm("Are you sure you want to reset KOKOMO?") ) {
          that.socket.emit('endGame', {screen_id: 1, team_one: {score: 0, points: 0}, team_two: {score: 0, points: 0}, finish_time: new Date(), game_uid: 0});
        }
      } else {
        if (confirm("Are you sure you want to reset BONDI?") ) {
          that.socket.emit('endGame', {screen_id: 0, team_one: {score: 0, points: 0}, team_two: {score: 0, points: 0}, finish_time: new Date(), game_uid: 0});
        }
      }
    }
  }

  getExtraPoints(teamScore) {
    if (teamScore >= 10 && teamScore < 20) {
      return 1;
    } else if (teamScore >= 20 && teamScore < 30) {
      return 2;
    } else if (teamScore >= 30 && teamScore < 40) {
      return 3;
    } else if (teamScore >= 40 && teamScore < 50) {
      return 4;
    } else if (teamScore >= 50 && teamScore < 60) {
      return 5;
    } else if (teamScore >= 60 && teamScore < 70) {
      return 6;
    } else if (teamScore >= 70 && teamScore < 80) {
      return 7;
    } else if (teamScore >= 80) {
      return 8;
    } else {
      return 0;
    }
  }

  gameTime(i) {
    let time = '';
    switch(i) {
      case 0:
        time = "6:00pm";
        break;
      case 1:
        time = "6:40pm";
        break;
      case 2:
        time = "7:20pm";
        break;
      case 3:
        time = "8:00pm";
        break;
      case 4:
        time = "8:40pm";
        break;
    }
    return time;
  }

  ngOnInit(): void {
  }

  selectField(field) {
    this.backButtonAvaliable = true;
    this.selectedField = field;
    this.socket.emit('selectField', {screen_id: field});
  }

  selectGame(game) {
    this.backButtonAvaliable = true;
    this.selectedGame = game;
    if(game[0].team_name == "Fill") {
      this.preGame("Fill", game[1].team_name, "", game[1].uid, this.selectedField, game[2]);
    } else if (game[1].team_name == "Fill") {
      this.preGame(game[0].team_name, "Fill", game[0].uid, "", this.selectedField, game[2]);
    } else {
      this.preGame(game[0].team_name, game[1].team_name, game[0].uid, game[1].uid, this.selectedField, game[2]);
    }
  }

  addDay(currentStartDate, days) {
    let currentDateObject = new Date(currentStartDate);
    let currentDate = currentDateObject.getDate();
    let pushDate = new Date(currentDateObject.setDate(currentDate + days));
    return (pushDate.getDate() < 10 ? '0' : '') + pushDate.getDate() + '/' + (pushDate.getMonth() < 10 ? '0' : '') + (pushDate.getMonth() + 1) + '/' + pushDate.getFullYear();
  }

}
