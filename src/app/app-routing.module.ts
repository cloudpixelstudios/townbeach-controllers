import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { ScoringComponent } from './pages/scoring/scoring.component';


const routes: Routes = [
  { path: 'scoring', component: ScoringComponent },
  { path: '',   redirectTo: '/scoring', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
