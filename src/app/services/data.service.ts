import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { take } from 'rxjs/operators';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  //Kokomo = screen 0;
  //Bondi = screen 1;


  //All games object - Updates on change to RTDB
  private seasonsSubject: BehaviorSubject<any> = new BehaviorSubject(undefined);
  public seasons: Observable<any> = this.seasonsSubject.asObservable();
  public staticSeasons;
  public staticTeams;

  constructor(private _database: AngularFireDatabase, private _auth: AngularFireAuth) {
    let that = this;
    that._database.list('season').valueChanges().subscribe(data => {
      that.staticSeasons = data;
      that.seasonsSubject.next(data);
    })
    that._database.object("teams").valueChanges().subscribe(data => {
      that.staticTeams = data;
    })
  }

  public convertDate(oldDate) {
    return (oldDate[3] + oldDate[4] + "/" + oldDate[0] + oldDate[1] + "/" + oldDate[6] + oldDate[7] + oldDate[8] + oldDate[9]);
  }

  public uploadResults(game_uid, team_one_points, team_two_points, team_one_score, team_two_score, team_one_uid, team_two_uid) {
    let that = this;
    return new Promise((resolve, reject) => {
      for(let week = 0; week < that.staticSeasons.length; week++) {
        let keys = Object.keys(that.staticSeasons[week]);
        for(let i = 0; i < keys.length; i++) {
          if(keys[i] != "startDate"){
            for(let j = 0; j < that.staticSeasons[week][keys[i]].length; j++) {
              if(that.staticSeasons[week][keys[i]][j].length < 5) {
                if(game_uid == that.staticSeasons[week][keys[i]][j][2]) {
                  let dbRef = that._database.list('season/' + week + "/" + keys[i] + "/" + j);
                  dbRef.update("4", { team_one: team_one_uid, team_one_score: team_one_score, team_one_points: team_one_points, team_two: team_two_uid, team_two_score: team_two_score, team_two_points: team_two_points }).then(() => {
                    let team_one_new_points = that.staticTeams[team_one_uid].points + team_one_points;
                    let team_two_new_points = that.staticTeams[team_two_uid].points + team_two_points;
                    let dbTeamRef1 = that._database.object("teams/" + team_one_uid);
                    let dbTeamRef2 = that._database.object("teams/" + team_two_uid);
                    dbTeamRef1.update({points: team_one_new_points}).then(() => {
                      dbTeamRef2.update({points: team_two_new_points}).then(() => {
                        resolve(true);
                      })
                    })
                  });
                }
              }
            }
          }
        }
      }
    });
  }
}
